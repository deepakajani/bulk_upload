import 'dart:ui';
import 'dart:io';
import 'dart:async';
import 'package:bulk_upload/Screens/SecondScreenView.dart';
import 'package:bulk_upload/Screens/ShowBulkImagesGridView.dart';
import 'package:bulk_upload/Screens/ShowProgressBar.dart';
import 'package:bulk_upload/Screens/UploadBulkImagesView.dart';
import 'package:bulk_upload/SelectImages.dart';
import 'package:bulk_upload/controllers/DataController.dart';
import 'package:bulk_upload/models/Items.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';
import 'package:provider/provider.dart';
import 'package:file_picker/file_picker.dart';
import 'package:searchable_dropdown/searchable_dropdown.dart';

class GalleryScreen extends StatefulWidget {
  final appBar;
  final showselectmediabutton;
  final showupdatemediabutton;
  final dataSet;
  final descriptionWidget;
  final showAlbum;
  final showAlbumData;

  GalleryScreen(
      {this.appBar,
      this.showselectmediabutton,
      this.showupdatemediabutton,
      this.dataSet,
      this.descriptionWidget,
      this.showAlbum,
      this.showAlbumData});

  @override
  _GalleryScreenState createState() => _GalleryScreenState();
}

class _GalleryScreenState extends State<GalleryScreen> {
  TextEditingController notesController = new TextEditingController();
  List<DropdownMenuItem> items = [];
  String selectedValue;
  String id;
  @override
  void initState() {
    notesController.clear();
    super.initState();
    if(widget.showAlbumData!=null)
    for (int i = 0; i < widget.showAlbumData.length; i++) {
      items.add(new DropdownMenuItem(
        child: new Text(
          widget.showAlbumData[i]['album_title'],
        ),
        value: widget.showAlbumData[i]['id'].toString(),
      ));
    }
   
  }

  @override
  Widget build(BuildContext context) {
    final showimages = Provider.of<DataModel>(context);
    print(showimages.isImageUploaded);
    return new Scaffold(
      backgroundColor: Colors.white,
      appBar: widget.appBar ??
          AppBar(
            backgroundColor: Colors.red,
            title: Text('Bulk Upload'),
          ),
      bottomNavigationBar: !showimages.isImageUploaded
          ? UploadBulkImages(
              widget.showupdatemediabutton,
              widget.dataSet,
              notes: notesController.text,
              albumId: id,
            )
          : ShowProgressBar(showimages: showimages),
      body: ListView(
        children: <Widget>[
          Visibility(
            visible: showimages.items.isEmpty,
            child: InkWell(
              onTap: () => SelectImages.getImages(context: context),
              child: widget.showselectmediabutton ??
                  Container(
                    padding: EdgeInsets.only(left: 20.0, right: 20.0),
                    width: MediaQuery.of(context).size.width,
                    child: Text(
                      "Select Media",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontSize: 25.0,
                      ),
                    ),
                  ),
            ),
          ),
          SizedBox(height: 20.0),
          Visibility(
            visible: showimages.items.isNotEmpty,
            child: Container(
              height: 300,
              width: MediaQuery.of(context).size.width,
              child: ShowBulkImagesGridView(),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left:8.0,right: 8,top: 8,bottom: 8),
            child: TextField(
              controller: notesController,
              maxLines: 5,
              onChanged: (value) {},
              cursorColor: Colors.black,
              autofocus: false,
              style: TextStyle(color: Colors.black, fontSize: 15),
              decoration: InputDecoration(
                  fillColor: Colors.transparent,
                  labelText: "Description",
                  filled: true,
                  labelStyle: TextStyle(color: Colors.black),
                  hintText: "Description...",
                  hintStyle: TextStyle(color: Colors.black, fontSize: 15),
                 
                  border: OutlineInputBorder(
                      borderSide: BorderSide(
                        width: 1,
                        style: BorderStyle.none,
                      ),
                      borderRadius: BorderRadius.all(Radius.circular(10.0)))),
            ),
          ),
          SizedBox(
            height: 10.0,
          ),
          Visibility(
            visible: widget.showAlbum,
            child: Container(height: 40,
              padding: EdgeInsets.only(left: 25, right: 25),
              width: MediaQuery.of(context).size.width,
              child: SearchableDropdown(
                isCaseSensitiveSearch: false,
                isExpanded: true,
                items: items,
                value: selectedValue,
                hint: Align(
                    alignment: Alignment.center,
                    child: Text(
                      'Album Name',
                      style: TextStyle(fontSize: 12, color: Colors.grey),
                    )),
                searchHint: Center(
                  child: Text(
                    'Album Name',
                    textAlign: TextAlign.center,
                    style: TextStyle(fontSize: 12, color: Colors.black),
                  ),
                ),
                onChanged: (value) {
                  print(value);
                  /* print(value);
                    print(widget.albums); */
                  selectedValue = value;
                  id = value;
                  setState(() {});
                },
              ),
            ),
          ),
        ],
      ),
    );
  }
}
