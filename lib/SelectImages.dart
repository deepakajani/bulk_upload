

import 'dart:io';

import 'package:file_picker/file_picker.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'controllers/DataController.dart';
import 'models/Items.dart';

class SelectImages  {
static getImages({context}) async {
   final showimages = Provider.of<DataModel>(context);

  List<File> resultList;

   try {
     
      resultList=await FilePicker.getMultiFile(type:FileType.IMAGE);

      if(resultList==null)return;
      if(resultList.length > 10){
        return showDialog(
       context: context,
       barrierDismissible: false, 
       builder: (BuildContext context) {
        return AlertDialog(title: Text('Cannot select more than 10'),
        actions: <Widget>[
          FlatButton(
            child: Text('OK'),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ],);
      });
    }

      showimages.isImageSelected = true;
      showimages.isImageUploaded = false;

  
    for (int i = 0; i < resultList.length; i++) {
       final Items items=Items(
        isupload: false,
        images: resultList[i],
        );
      showimages.addImageIntoItems(images: items);
      }

     }  catch (e) {
      print("Image picker "+ e.toString());
    }


  }
}