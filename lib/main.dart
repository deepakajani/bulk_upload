library bulk_upload;

import 'package:bulk_upload/bulk_upload.dart';
import 'package:bulk_upload/controllers/DataController.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class MainFile extends StatelessWidget {
  final appBar;
  final showButton;
  final showDescrition;
  final dataSetForUpload;
  final upload;
  final showAlbum;
  final albumData;

  

  const MainFile({Key key, this.appBar, this.showButton, this.showDescrition, this.dataSetForUpload, this.upload, this.showAlbum, this.albumData}) : super(key: key);

  @override
  Widget build(BuildContext context) {
   return MultiProvider(providers: [
        ChangeNotifierProvider<DataModel>(create: (_)=>DataModel()),
    ],
    child: Consumer<DataModel>(
      builder:(context,dataModel,child){     
          return GalleryScreen(appBar: appBar,showselectmediabutton: showButton,dataSet: dataSetForUpload,showupdatemediabutton: upload,showAlbum: showAlbum,showAlbumData: albumData,);}
      ));
     }
   
  
}