import 'package:bulk_upload/controllers/DataController.dart';
import 'package:bulk_upload/models/Items.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:carousel_widget/carousel_widget.dart';
import 'package:carousel_slider/carousel_slider.dart';

class ShowBulkImagesGridView extends StatefulWidget {
  @override
  _ShowBulkImagesGridViewState createState() => _ShowBulkImagesGridViewState();
}

class _ShowBulkImagesGridViewState extends State<ShowBulkImagesGridView> {

   int _current = 0;
   
  @override
  Widget build(BuildContext context) {
    final showimages = Provider.of<DataModel>(context);
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child:showimages.items.length==null?Container(): Stack(
        children: [
          CarouselSlider( 
          height: 200,
          initialPage: 0,
          enableInfiniteScroll: false,
          reverse: false,
          autoPlay: true,
          autoPlayInterval: Duration(seconds: 3),
          autoPlayAnimationDuration: Duration(milliseconds: 800),
          autoPlayCurve: Curves.fastOutSlowIn,
          enlargeCenterPage: true,
          scrollDirection: Axis.horizontal,
          onPageChanged: (index){
              setState(() {
                _current=index;
              });
          },
            items: List.generate(showimages.items.length , (index){
                return Stack(
                  children: [
                    Opacity(
                      opacity: showimages.items[index].isupload ? 1.0 : 0.5,
                      child: Container(
                        child: Image.file(showimages.items[index].images,
                           fit: BoxFit.cover),
                      ),
                    ),
                    Visibility(
                      visible: showimages.items[index].isupload ? false : true,
                      child: Positioned(
                        top: 0.0,
                        right: 0.0,
                        child: Container(
                          height: 25.0,
                          width: 25.0,
                          decoration: BoxDecoration(
                              color: Colors.red, shape: BoxShape.circle),
                          child: IconButton(
                            icon: Icon(
                              Icons.cancel,
                              color: Colors.white,
                            ),
                            onPressed: () {
                              showimages.deleteImages(
                                  itemsdata: showimages.getallitems[index]);
                            },
                            padding: EdgeInsets.only(bottom: 10.0),
                          ),
                        ),
                      ),
                    ),
                  ],
                );
              }),
           ),
            Positioned(
              bottom: 0.0,
              left: 0.0,
              right: 0.0,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: List.generate(showimages.items.length, (index) {
                  return Container(
                    width: 8.0,
                    height: 8.0,
                    margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 2.0),
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: _current == index ? Color.fromRGBO(0, 0, 0, 0.9) : Color.fromRGBO(0, 0, 0, 0.4)
                    ),
                  );
                }),
              )
            )
        ],
      ),
    );
  }
}
