import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';
import 'package:provider/provider.dart';

import '../controllers/DataController.dart';
import 'SecondScreenView.dart';

class ShowProgressBar extends StatefulWidget {
  final showimages;

  const ShowProgressBar({Key key, this.showimages}) : super(key: key);
  @override
  _ShowProgressBarState createState() => _ShowProgressBarState();
}

class _ShowProgressBarState extends State<ShowProgressBar> {
  @override
  Widget build(BuildContext context) {
    final showimages = Provider.of<DataModel>(context);
    return Container(
      height: 80,
      child: Scaffold(
        bottomNavigationBar: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Container(
            child: Wrap(
                spacing: 8.0,
                runSpacing: 4.0,
                direction: Axis.horizontal,
                children: <Widget>[
                  Text("${showimages.counter}/${showimages.items.length}",
                      style: TextStyle(
                        fontSize: 16.0,
                        color: Colors.grey[600],
                      )),
                  new LinearPercentIndicator(
                    width: MediaQuery.of(context).size.width - 50,
                    animation: false,
                    lineHeight: 20.0,
                    percent: showimages.progress,
                    center: Text('${showimages.progresspercent}%',style:TextStyle(color:Colors.white)),
                    linearStrokeCap: LinearStrokeCap.roundAll,
                    backgroundColor: Colors.grey,
                    progressColor: Colors.green,
                  ),
                ]),
          ),
        ),
      ),
    );
  }
}
