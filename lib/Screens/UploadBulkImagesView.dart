import 'dart:io';
import 'package:bulk_upload/controllers/DataController.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:provider/provider.dart';

class UploadBulkImages extends StatelessWidget {
  final showupdatemediabutton;
  final dataSet;
  final notes;
  final albumId;
  UploadBulkImages(this.showupdatemediabutton, this.dataSet,
      {this.notes, this.albumId});

  @override
  Widget build(BuildContext context) {
    final uploadimages = Provider.of<DataModel>(context);
    return Visibility(
      visible: uploadimages.isImageSelected &&
          !uploadimages.isImageUploaded &&
          uploadimages.items.isNotEmpty,
      child: InkWell(
        onTap: () async {
          try {
            final result = await InternetAddress.lookup('google.com');
            if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
              print('connected');

              if (albumId == null && dataSet["albumId"] == null) {
                return Fluttertoast.showToast(
                    msg: "Please select album",
                    toastLength: Toast.LENGTH_SHORT,
                    gravity: ToastGravity.BOTTOM,
                    timeInSecForIos: 1,
                    backgroundColor: Colors.black54,
                    textColor: Colors.white,
                    fontSize: 16.0);
              }
              uploadimages
                  .uploadmultipleimages(
                      uploadimages.items,
                      dataSet["header"],
                      dataSet["albumId"] == null ? albumId : dataSet["albumId"],
                      dataSet["api"],
                      context,
                      notes: notes,
              )
                  .then((onValue) {
                if (onValue) {
                  uploadimages.initialize();
                }
              });
              // Navigator.push(context, MaterialPageRoute(builder: (context)=>SecondScreen()));
            }
          } on SocketException catch (_) {
            print('not connected');
            Fluttertoast.showToast(
                msg: "No connection",
                toastLength: Toast.LENGTH_SHORT,
                gravity: ToastGravity.BOTTOM,
                timeInSecForIos: 3,
                textColor: Colors.black,
                backgroundColor: Colors.grey,
                fontSize: 18.0);
          }
        },
        child: showupdatemediabutton ??
            Container(
              padding: EdgeInsets.only(left: 15.0, right: 15.0, bottom: 50.0),
              width: MediaQuery.of(context).size.width,
              child: Text(
                'Upload',
                style: TextStyle(
                  fontSize: 25.0,
                ),
              ),
            ),
      ),
    );
  }
}
