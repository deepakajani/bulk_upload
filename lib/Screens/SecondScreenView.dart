import 'package:bulk_upload/controllers/DataController.dart';
import 'package:flutter/material.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';
import 'package:provider/provider.dart';


class SecondScreen extends StatefulWidget{




  @override
  _SecondScreenState createState() => _SecondScreenState();
}

class _SecondScreenState extends State<SecondScreen> {

  @override
  Widget build(BuildContext context) {
  
  final showprogress=Provider.of<DataModel>(context);

        return Scaffold(
     backgroundColor: Colors.white,
      appBar: PreferredSize(preferredSize: Size(MediaQuery.of(context).size.width,200),child: Padding(
       padding: const EdgeInsets.only(top: 50.0,bottom: 20.0),
       child: Center(
               child:  buildLinearProgressIndicator(context:context,showprogress: showprogress),
               ),
     ),),
     body:  SingleChildScrollView(
         child: Center(
           child: Wrap(
            spacing: 4.0,
            runSpacing: 4.0,
             children:   List.generate(showprogress.items.length, (index) {
              return Stack(
                children: [
                  Opacity(
                    opacity: showprogress.items[index].isupload ? 1.0 : 0.5,
                    child: Container(
                      child: Image.file(showprogress.items[index].images,
                          width: 200, height: 200, fit: BoxFit.cover),
                     ),
                   ),
                 ]);
               }),
             ),
         ),
      ));
 
  }

   Widget buildLinearProgressIndicator({context,showprogress}) {
            return  Center(
              child: Container(
                padding: EdgeInsets.only(left:30.0),
                child: Wrap(
                  spacing: 8.0,
                  runSpacing: 4.0,
                  direction: Axis.horizontal,
                  children: <Widget>[
                    Text('${showprogress.progresspercent}%',
                    style: TextStyle(
                      fontSize: 16.0,
                      color: Colors.grey[600],
                     ) ),
                    new LinearPercentIndicator(
                      width: MediaQuery.of(context).size.width - 80,
                      animation: false,
                      lineHeight: 20.0,
                      percent: showprogress.progress,
                      center: Text("${showprogress.counter}"),
                      linearStrokeCap: LinearStrokeCap.roundAll,
                      backgroundColor: Colors.grey,
                      progressColor: Colors.green,
                    ),
                    RaisedButton(
                      onPressed: (){
                        Navigator.pop(context);
                      },
                      child: Text('OK'),
                    ),
                  ],
                ),
              ),
            );
  }
}