
import 'package:flutter/material.dart';
import 'package:progress_dialog/progress_dialog.dart';



class Utils {
  ProgressDialog initDialog(BuildContext context,) {
    return new ProgressDialog(context,
        type: ProgressDialogType.Normal, isDismissible: true, showLogs: true);
  }
}
