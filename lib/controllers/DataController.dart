import 'dart:io';
import 'dart:async';
import 'dart:js';
import 'package:bulk_upload/Utills.dart';
import 'package:bulk_upload/models/Items.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:flutter/cupertino.dart';

class DataModel extends ChangeNotifier {
  List<Items> items = [];
  int counter = 0;
  double progress = 0.0;
  int progresspercent = 0;
  bool isImageSelected = false;
  bool isImageUploaded = false;

  get getallitems => items;

  set setisImageUploaded(bool val) {
    isImageUploaded = val;
  }

  initialize() {
    items = [];
    counter = 0;
    progress = 0.0;
    progresspercent = 0;
    isImageSelected = false;
    isImageUploaded = false;
    notifyListeners();
  }

  addImageIntoItems({@required images}) {
    items.add(images);
    notifyListeners();
  }

  Future uploadmultipleimages(images, header, albumId, api,context, {notes}) async {
    Utils().initDialog(context).show();
    for (int i = 0; i < items.length; i++) {
      if (items[i].isupload == false) {
        isImageUploaded = true;
        File imageFile = items[i].images;
        
        var response = await uploadApiCall(imageFile, header, albumId, api,
            remain: items.length - i, notes: notes);
        if (response == 200) {
          counter += 1;
          progress = (counter / images.length).toDouble();
          progresspercent = (progress * 100.0).toInt();
          items[i].isupload = true;
          notifyListeners();
          print("Images Uploaded");
          Fluttertoast.showToast(
              msg: "Image uploaded",
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.BOTTOM,
              timeInSecForIos: 1,
              backgroundColor: Colors.black54,
              textColor: Colors.white,
              fontSize: 16.0);
        } else {
          print("Upload Failed");
        }
      }
    }
    Utils().initDialog(context).hide();
    notifyListeners();
    return true;
  }

  deleteImages({Items itemsdata}) {
    items.remove(itemsdata);
    notifyListeners();
  }

  Future uploadApiCall(image, header, albumId, api, {remain, notes}) async {
    print(remain);
    /* 
    FormData formdata = new FormData(); // just like JS
    formdata.add("photos", mediafile.path); */
    var data = image.toString().split(".");
    int length = data.length;

    var formdata = FormData.fromMap(<String, dynamic>{
      "gallery_album_id": albumId,
      "media_type": "Image",
      "notes": notes,
      "media_file": await MultipartFile.fromFile(image.path,
          filename: "mediafile.${data[length - 1].replaceAll("\'", "")}"),
      'remaining_media': remain - 1
    });

    Dio dio = new Dio();
    Response response;
    try {
      response = await dio.post(api,
          data: formdata, options: Options(headers: header));
      print("response is:$response");
      return response.statusCode;
    } on DioError catch (e) {
      print('error in' + e.response.toString());
    }
  }
}
